let view1 = document.getElementById("view1"), view2 = document.getElementById("view2"), view3 = document.getElementById("view3"), btn = document.getElementById("btn"), form = document.getElementById('form');

btn.addEventListener("click", (e) => {
    form.style.display = 'block'
})
function slide() {
    setTimeout( function () {
        view1.classList.add("dissertation1");
        view1.classList.remove("dissertation3");
        view2.classList.add("coursework1");
        view2.classList.remove("coursework3");
        view3.classList.add("review1");
        view3.classList.remove("review3");
    }, 5000)
    setTimeout( function () {
        view1.classList.add("dissertation2");
        view1.classList.remove("dissertation1");
        view2.classList.add("coursework2");
        view2.classList.remove("coursework1");
        view3.classList.add("review2");
        view3.classList.remove("review1");
    }, 10000)
    setTimeout( function () {
        view1.classList.add("dissertation3");
        view1.classList.remove("dissertation2");
        view2.classList.add("coursework3");
        view2.classList.remove("coursework2");
        view3.classList.add("review3");
        view3.classList.remove("review2");
    }, 15000)
}

slide();
setInterval(slide, 15000);